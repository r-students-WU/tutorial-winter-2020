# Repository for the VW-SozÖk-Seep R-Tutorial Winter 2020

## Dates & Location

Time: always 18:00 - 20:00 (6pm - 8pm)

* Monday, October 19
* Monday, November 2
* Monday, November 9
* Monday, November 16

via MS Teams: https://teams.microsoft.com/l/channel/19%3a327200012dc14dbd841564f7381a0037%40thread.tacv2/General?groupId=7c500954-683b-4f5f-9ed0-78a687c4f8d6&tenantId=0504f721-d451-402b-b884-381428559e39

## Software downloads

***You don't have to install anything beforehands - We'll set up R at the beginning of the first session***

### R

- Debian/Ubuntu: `sudo apt install r-base`
    - also see `https://gitlab.com/r-students-WU/DotfilesAndOtherNonRcode/tree/master/ubuntu18_04`

- RHEL/CentOS/Fedora: `sudo yum install R`

- (open)SUSE: https://cran.r-project.org/bin/linux/suse/

- MacOS: https://cran.r-project.org/bin/macosx/

- Windows: https://cran.r-project.org/bin/windows/

### R-Studio

https://www.rstudio.com/products/rstudio/download/#download

### Git

https://git-scm.com/downloads

### Other Resources

https://imsmwu.github.io/MRDA2018/_book/index.html

